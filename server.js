//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var requestjson = require('request-json');
var urlMovimientosMlab = "https://api.mlab.com/api/1/databases/ochigo/collections/movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMlab = requestjson.createClient(urlMovimientosMlab);

var path = require('path');
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
var movimientosJSON = require('./movimientosv2.json');
app.get('/',function(req, res) {
  //res.send("Hola mundo");
  res.sendFile(path.join(__dirname,'index.html'));
});

app.get('/Clientes', function(req, res){
  res.send("Aqui están los clientes owo : TwT");
});

app.get('/Clientes/:idCliente', function(req, res){
  res.send("Este es el cliente "+req.params.idCliente);
});

app.get('/Movimientos/V1',function(req, res){
  res.sendfile('movimientosv1.json');
});

app.get('/Movimientos/V2',function(req, res){
  res.send(movimientosJSON);
});

app.get('/Movimientos/V2/:index',function(req, res){
  console.log(req.params.index);
  console.log(req.params.index-1);
  res.send(movimientosJSON[req.params.index-1]);
});

app.get('/Movimientosq/V2',function(req, res){
  console.log(req.query);
  res.send('recibido');
});

app.post('/', function(req, res){
  res.send("Hemos recibido su petición post");
});

app.post('/Movimientos/V2', function(req, res){
  var nuevo= req.body;
  nuevo.id= movimientosJSON.length+1;
  movimientosJSON.push(nuevo);
  console.log(nuevo);
  res.send("Movimiento dado de alta");
});

app.get('/Movimientos', function(req, res){
  clienteMlab.get('', function(err, resM, body){
    if(err){
      console.log(body);
    }
    else{
      res.send(body);
    }
  })
})

app.post('/Movimientos', function(req, res){
  clienteMlab.post('', req.body, function(err, resM, body){
    if (err)
      console.log(body);
    else
      res.send(body);
  })
});
